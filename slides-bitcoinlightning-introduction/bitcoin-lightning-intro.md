<!-- https://mermaid-js.github.io/mermaid-live-editor/ --> 
<!-- fg=green,bg=black -->

# Bitcoin Lightning

Schnell, preiswert, dezentral, anonym?

https://lightning.network/

---
<!-- fg=green,bg=black -->

## Einführung

* Welches Problem soll Bitcoin Lightning lösen?
* die grundlegende Idee dahinter
* ein kurzer historischer Rückblick
* Wo steht Bitcoin Lightning heute?

---
<!-- fg=green,bg=black -->

### Welches Problem soll Bitcoin Lightning lösen?

* die Sicherheit des Bitcoin-Netzwerkes bringt Nachteile bei der Zahlungsabwicklung
	* Anzahl der Zahlungen pro Sekunde zu gering
	* Gebühr bei kleinen Beträgen zu hoch
	* einzelne Zahlung nicht sofort abgeschlossen

---
<!-- fg=green,bg=black -->

### Die Idee dahinter - Kanal

* zwei Nutzer erzeugen einen Kanal und legen jeweils einen Betrag an eines der Enden das Kanals
* wenn der Kanal geöffnet ist, können Beträge von der einen Seite zur anderen Seite transferiert werden
* wird der Kanal geschlossen, bekommen die Nutzer den Betrag ihrer Seite des Kanals gut geschrieben

![RC](images/channel1.png)
<!-- graph TD
    A(Alice - 10sat) --- |Channel| B(Bob - 5sat)  -->

---
<!-- fg=green,bg=black -->
### Die Idee dahinter - Routen

* Zahlungen können gegen eine Gebühr geroutet werden

![RC](images/channel2.png)
<!-- graph LR
    A(Alice - 10sat) --- |Channel| B(Bob - 5sat) --- |Channel| C(Kim - 4sat) -->

---
<!-- fg=green,bg=black -->
### Historischer Rückblick

* die grundlegende Idee geht auf Satoshi Nakamoto zurück
* mit der vermehrten Nutzung von Bitcoin wurde das Problem der Skalierbarkeit drängender
* ab 2016 entstanden erste Implementierungen (lnd, c-lightning, eclair)
* zur effizienten Umsetzung brauchte man das Segregated-Witness-Update (2017)
* Rusty Russel entwickelte auf Grundlage des Whitepapers einen RFC-Standard für das Lightning Netzwerk

---
<!-- fg=green,bg=black -->

### Wo steht Bitcoin Lightning heute?

* über 16000 öffentliche Knoten - https://explorer.acinq.co
* Beta-Status
* sehr aktive Community
* im produktiven Einsatz in El Salvador
* Entwicklungsbedarf beim Routing
* Pickardt Payments

---
<!-- fg=green,bg=black -->

## Aufbau

* Wie funktioniert es? (etwas detailierter)

---
<!-- fg=green,bg=black -->

### Funktionsweise

* es gibt die genannten Zahlungskanäle (uni- bidirektionale)
* mit Hilfe des Kanals können sich zwei Knoten durch Benutzung einer 2-2-Multisignatur-Wallet Geldbeträge hin- und herschicken

---
<!-- fg=green,bg=black -->

* der Kanal wird durch eine Funding-Transaktion geöffnet
* nach jeder Zahlung wird der aktuelle Zustand in einer Commitment-Transaktion festgehalten
* schließt eine(r) den Kanal wird eine Settlement-Transaktion veröffentlicht

---
<!-- fg=green,bg=black -->

* diese speichert den finalen Saldo beider Parteien aus der letzten Commitment-Transaktion in der Bitcoin-Blockchain
* das Protokoll zur Verwaltung eines Kanals ist mithilfe von HTLCs konstruiert und soll betrügerisches Verhalten bestrafen

---
<!-- fg=green,bg=black -->

Hashed Timelock Contract (HTLC)

* ist eine Klasse von Transaktionen
* der Sender muss entweder einen kryptographischen Beweis vorlegen oder bis zu einem bestimmten Datum warten

---
<!-- fg=green,bg=black -->

* besteht aus zwei Komponenten:
    * Geheimnis (Secret) - zufällige Zahl (Pre Image) welche gehasht ist
    * Zeitschloss

---
<!-- fg=green,bg=black -->

* wer das Pre Image kennt besitzt den kryptographischen Beweis um die Transaktion zu tätigen
* werden in bidrektionalen Kanälen verwendet

---
<!-- fg=green,bg=black -->

### Routing

* ermöglicht Zahlungen zwischen beliebigen Knoten, wenn es eine Route gibt
* Idee des Onion-Routing:
    * Sender muss zuerst Pfad ermitteln
    * für jeden Hop können Transaktionen verschachtelt werden
* Knoten können für die Dienstleistung des Geld weiterleitens eine Gebühr erheben
* die Gebühr wird über das Gossip-Protokoll dem Netzwerk mitgeteilt

---
<!-- fg=green,bg=black -->


## Verwendung

* lntxbot
* Bluewallet
* Phoenix
* RaspiBlitz
* Casa Node
* Sphinx
* Podcast 2.0

---
<!-- fg=green,bg=black -->

## Diskussion

* Dezentralität
* Sicherheit
* Entwicklungsstand
* Offline-Zahlungen
* Privatsphäre

---
<!-- fg=green,bg=black -->

## Links

* https://lightning.network
* https://github.com/lightning/bolts
* https://github.com/fiatjaf/lnurl-rfc
* https://explorer.acinq.co/
* https://github.com/rootzoll/raspiblitz

